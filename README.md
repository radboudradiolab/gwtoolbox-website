# GWToolbox Website

This repository contains a website that demonstrates the use of the gwtoolbox code found in the main repo:
http://bitbucket.org/radboudradiolab/gwtoolbox

## Cloning the repo
The gwtoolbox-website contains the gwtoolbox repo as a git submodule.
When cloning the repo add --recursive to automatically fetch a copy of the sources if you don't have them globally installed in your system already.

## Installation
Make sure to update the hard-coded paths to finesse, kat and the gwtoolbox module if you did not globally install it on your system.
