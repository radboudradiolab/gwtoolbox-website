Tools
=====

Standard usage of gwtoolbox involves instantiating Tools from Earth or space

Earth
-----

.. automodule:: gwtoolbox.tools_earth

  .. autoclass:: gwtoolbox.tools_earth.Tools
     :members:

Space
-----

.. automodule:: gwtoolbox.tools_space

  .. autoclass:: gwtoolbox.tools_space.Tools
     :members: