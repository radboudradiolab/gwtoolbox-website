Instalation
===========

From source
-----------

install FINESSE (detector simulation software): see http://www.gwoptics.org/finesse/#download

install PyKat (Python interface to FINESSE): see https://pypi.org/project/PyKat/

install GWToolbox - clone the repository and run the installation: 

.. code-block:: python

    python setup.py install

Before running, make sure that:

FINESSE_DIR is set correctly

KATINI dir is set correctly

PYTHONPATH is set correctly
