Sources
=======

kHz
---

From Earth we can detect following systems: stellar mass black hole mergers, binary neutron star mergers, stellar mass black hole - neutron star mergers. Each system is represented by its own class.

.. automodule:: gwtoolbox.sources_kHz

  .. autoclass:: gwtoolbox.sources_kHz.BHB
     :members:

  .. autoclass:: gwtoolbox.sources_kHz.DNS
     :members:

  .. autoclass:: gwtoolbox.sources_kHz.BHNS
     :members:

mHz  
---

From space we can observe: super massive black hole mergers, inspirals of binary stellar black hole mergers, inspirals of double neutron star mergers, inspirals of stellar black hole-neutron star mergers, and extreme mass ratio inspirals.

.. automodule:: gwtoolbox.sources_mHz

  .. autoclass:: gwtoolbox.sources_mHz.SMBHB
     :members:

  .. autoclass:: gwtoolbox.sources_mHz.EMRI
     :members:

  .. autoclass:: gwtoolbox.sources_mHz.Insp
     :members:

nHz
---

