#!/usr/local/bin/python3
#/var/www2/gwuniverse/anaconda3/bin/python3.7
# this shebang line replaces the path on the server. when developping locally
# the addition to the path will simply be ignored.
from multiprocessing import freeze_support
import recaptcha
import time
tick=time.time()
import numpy as np
import pandas as pd
import os
import json
if __name__=='__main__':
    freeze_support();
    if os.getenv("FINESSE_DIR") is not None:
        pass
    else:
        # the path is the server
        os.environ["FINESSE_DIR"] = "/var/www2/gwuniverse/anaconda3/bin/"
    if os.getenv("KATINI") is not None:
        pass
    else:
        # the path in the server
        os.environ["KATINI"] = "/var/www2/gwuniverse/anaconda3/bin/"
    import cgi
    import sys
    # the path to the main codes
    sys.path.insert(0, '../gwtoolbox')

    from astropy.constants import c
    from gwtoolbox.tools_space import Tools, set_cosmology
    from gwtoolbox import parameters
    from gwtoolbox import plotshtml as gwplots
    from gwtoolbox.sources_mHz import *

    def format_response(response):
        """ Prints the content-type header and formts the data as json"""
        print('Content-type:text/json\r\n\r\n')
        json.dump(response, sys.stdout)
        quit()

    def error_response(msg):
        format_response({'msg': msg, 'success': False})

    def html_start():
        print ('Content-type:text/html\r\n\r\n')
        print ('<html>')
        print ('<head>')
        #print()
        print ('<title>GW Toolbox - results</title>')
        print ('</head>')
        print ('<body>')

    def html_start_txt():
        print ('Content-type: text/ascii \r\n\r\n')


    def html_end():
        print ('<br>')
        print ('</body>')
        print ('</html>')

    form = cgi.FieldStorage()

    tock=time.time()
    time_initialize=tock-tick
#    html_start()
#    print("starting")
#    html_end()
    if not recaptcha.verify(form.getvalue('recaptcha')):
        html_start()
        print("You're requests are deemed too robot-like. Aborting.")
        html_end()
        quit()
    _cosmology = form.getvalue('cosmo_model') # string
    _obs_time = form.getvalue('obs_time') # float, read in days, convert to minutes
    _detector_id  = form.getvalue('detector_name') # 0 1 -1 2 -2
    _snr = form.getvalue('signal_to_noise') # float
    _det_arm = form.getvalue('det_arm') # float
    _det_laser_power = form.getvalue('det_laser_power') # float
    _det_dia = form.getvalue('det_dia') # float
    if form.getvalue('Sacc0')!=None:
        _det_sacc0=float(form.getvalue('Sacc0')) # float
    #_det_sacc0=1e-4
    else:
        _det_sacc0=form.getvalue('Sacc0') # float
    if form.getvalue('Sops')!=None:
        _det_sops0=float(form.getvalue('Sops')) # float
    else:
        _det_sops0=form.getvalue('Sopo0')
    if form.getvalue('Sopo0'):
        _det_sopo0=float(form.getvalue('Sopo0')) # float
    else:
        _det_sopo0=form.getvalue('Sopo0')
    #_mbhb = form.getvalue('mbhb') # 1
    _sourcetype_mHz=form.getvalue('sourcetype_mHz')
    _mbhb_pop = form.getvalue('mbhb_pop') # 0 1
    #_gb = form.getvalue('gb') # 1
    _gwd_pop = form.getvalue('gwd_pop') # 0 1
    #_emri = form.getvalue('emri') # 1
    _emri_pop = form.getvalue('emri_pop') # 0 1

    _output = form.getvalue('output') # string
    if _output == "table":
        _sample_size =  form.getvalue('output_size_table')
    elif _output == "table2":
        _sample_size =  form.getvalue('output_size_table2')
    #elif _output == "plot_vs":
    #    _sample_size =  form.getvalue('output_size_plot')
    #
    if '_cosmology' in locals():
       if _cosmology in ['FlatLambdaCDM','WMAP5','WMAP7','WMAP9','Planck13','Planck15']:
         cosmology = _cosmology
       else:
         quit()

    if '_obs_time' in locals():
     if _obs_time != None: 
      try:
        obs_time = float(_obs_time)*0.00273790926 # read in days, convert into years
      except:
        quit()
     else:
        obs_time = parameters.TIME_OBS_EARTH

        
    if '_snr' in locals():
     if _snr != None:
      try:
        snr = float(_snr)
      except:
        quit()
     else:
        snr = parameters.RHO_CRIT_EARTH

    if '_detector_id' in locals():
      try:
        detector_id = int(_detector_id)
        if detector_id not in [-1, 0]:
          quit() 
      except:
        html_start()
        print ('Please choose a detector')
        html_end()
        quit()


    if '_det_arm' in locals():
     if _det_arm != None:
      try:
        det_arm = float(_det_arm)
      except:
        quit()

    if '_det_laser_power' in locals():
     if _det_laser_power != None:
      try:
        det_laser_power = float(_det_laser_power)
      except:
        quit()

    if '_det_dia' in locals():
        if _det_dia != None:
            try:
                det_dia = float(_det_dia)
            except:
                quit()
    if '_det_sacc0' in locals():
        if _det_sacc0 != None:
            try:
                det_sacc0 = float(_det_sacc0)
            except:
                quit()

    if '_det_sops0' in locals():
        if _det_sops0 != None:
            try:
                det_sops0 = _det_sops0
            except:
                quit()

    if '_det_sopo0' in locals():
        if _det_sopo0 != None:
            try:
                det_sopo0 = _det_sopo0
            except:
                quit()


    if _sourcetype_mHz=='1':
        if _mbhb_pop=='1':
            populations=0.1
        elif _mbhb_pop=='2':
            populations=0.2
        elif _mbhb_pop=='3':
            populations=0.3
        #populations=0.+float(_mbhb_pop)*0.1
    elif _sourcetype_mHz=='2':
        populations=1.+float(_gwd_pop)*0.1
    elif _sourcetype_mHz=='3':
        populations=2.+float(_emri_pop)*0.01
    else : pass
    #if '_mbhb' in locals() and _mbhb != None:
    #  if int(_mbhb) == 1:
    #      populations.append(float('0.'+_mbhb_model))
    #if '_gb' in locals() and _gb != None:
    #  if int(_gb) == 1:
    #    populations.append(float('1.'+_gb_model))
    #if '_emri' in locals() and _emri != None:
    #  if int(_emri) == 1:
    #    populations.append(float('2.'+_emri_model))

    #population model not specified yet

    output_total = output_table = output_table2= False
    if '_output' in locals():
    #  if _output == 'sensfun':
         #output_sensfun = True     
        if _output == 'number':
            output_total = True
        elif _output == 'table':
            output_table = True
        elif _output == 'table2':
            output_table2= True
        #elif _output == 'plot_vs':
        #   output_plot_vs = True
        else:
            html_start()
            print ('Please choose an output type')
            html_end()
            quit()
    else:
        quit()

    tick=time.time()
    time_readinform=tick-tock

    if output_table or output_table2:
        if '_sample_size' in locals():
            try:
                sample_size = int(_sample_size)
            except:
                sample_size = parameters.SAMPLE_SIZE
    #
    #  desired_columns = []
    #  for field in form.list:
    #      if field.name.startswith('tab_'):
    #          desired_columns.append(field.name[4:])
    # 
    #  if output_table:
    #    if 'mass1' in desired_columns and 'mass2' not in desired_columns:
    #        desired_columns.append('mass2')
    #    if 'mass2' in desired_columns and 'mass1' not in desired_columns:
    #        desired_columns.append('mass1')
    #
    #  elif output_plot_vs: # TODO, fix from here
    #    for field in form.list:
    #      if 'plot_vs' in field.name:
    #        ifield=int(field.value)
    #        if ifield > 99: errors=True
    #        output_param_list.append(ifield)
    #        
      #elif output_plot_hist:
      #  for field in form.list:
      #    if 'plot_hist' in field.name: 
      #      ifield=int(field.value)
      #      if ifield > 99: errors=True
      #      output_param_list.append(ifield)

    # set cosmology
    if cosmology in ['WMAP5','WMAP7','WMAP9','Planck13','Planck15']:
        cosmos=set_cosmology(cosmology)
    elif cosmology=='FlatLambdaCDM':
        H0=float(form.getvalue('H0'))
        Om0=float(form.getvalue('omega'))
        Tcmb=float(form.getvalue('Tcmb'))
        cosmos=set_cosmology(cosmoID=cosmology,H0=H0,Om0=Om0,Tcmb=Tcmb)
    tock=time.time()
    time_setoutput_cosmos=tock-tick

    # plot sensitivity function
    #if output_sensfun:
        #html_start()
    def plotsensecurve():
        print ('The noise power density in TDI-X channel:'+'<br>')
        from numpy import sqrt
        from gwtoolbox.detectors_space import LisaLike
        if detector_id == 0:
            # 0 is virgo, 1 is ligo, 4 is kagra
            new_det = LisaLike()
        else:
            new_det = LisaLike(lisaLT=det_arm/c.value, lisaD=det_dia, lisaP=det_laser_power, Sacc0=det_sacc0, Sopo0=det_sopo0, Sops0=det_sops0)
        x=np.logspace(-5,0,500)
        #x=np.linspace(1e-5,1,500)
        y = new_det.Stdix(x)
        uri = gwplots.plot_Stdix(x,sqrt(y))
        print ('<img src = "'+uri+'" width="500">')
        return None
    from numpy import sqrt
    from gwtoolbox.detectors_space import LisaLike
    if detector_id == 0:
            # 0 is virgo, 1 is ligo, 4 is kagra
        new_det = LisaLike(Tobs=obs_time)
    else:
        new_det = LisaLike(lisaLT=det_arm/c.value, lisaD=det_dia, lisaP=det_laser_power, Sacc0=det_sacc0, Sopo0=det_sopo0, Sops0=det_sops0, Tobs=obs_time)
    frequencies=np.logspace(-5,0,500)
        #x=np.linspace(1e-5,1,500)
    noises = new_det.S_michaelson(frequencies)
    noises_sqrt=np.sqrt(noises)
    response= {} # respose in a json
    response['success'] = True
    response['sensitivity'] = [frequencies.tolist(), noises_sqrt.tolist()]
    # print total number of detected sources
    if output_total:   
    #  if len(populations) == 0:
    #    html_start()
    #    print ('Please choose source population(s)')
    #    html_end()
    #    quit()
        html_start()
        print ('Input: <br>')  
        print ('Detector: LISA-like<br>') 
        plotsensecurve() 
        print ('<br>')
        print ('SNR: '+str(snr)+'<br>')
        print ('Observing time: '+str(int(obs_time))+' mins <br>')
        Tyears = obs_time
        if Tyears < 0.1 or Tyears > 10:
            html_start()
            print ('Please set observing time between 0.1 and 10 years.')
            html_end()
            quit()
        print ('<br>')
        print ('Output: <br>')
        if (detector_id < 0):
            det_setup=[det_arm/c.value, det_laser_power, det_dia]
        else:
            det_setup=[8.3391, 1.0, 0.25]
        #for ipop in populations:
        print ('Population: '+mHz_models[populations]+'<br>')
        t=Tools(Tobs=Tyears, popID=populations,cosmos=cosmos, det_setup=det_setup)
        print('Total number of detections: ',int(t.total(rho_cri=snr)),'<br>')
        html_end()

    # print table with parameters
    elif output_table:
        tick=time.time()
    #  if len(populations) == 0 or len(desired_columns) == 0:
    #    html_start()
    #    print ('Choose source population(s) and/or parameter(s)!')
    #    html_end()
    #    quit()    
#        html_start()
#        print ('Input: <br>')  
#        print ('Detector: LISA-Like<br>')
#        #plotsensecurve()  
#        print ('<br>')
#        print ('SNR: '+str(snr)+'<br>')
        Tyears = obs_time 
#        print ('Time of observation: %f years' % Tyears+'<br>')
        if Tyears < 0.1 or Tyears >= 10:
            html_start()
            print ('Please set observing time between 0.1 and 10 years.')
            html_end()
            quit()
        #print ('Output: <br>')
        if (detector_id < 0):
            det_setup=[det_arm / c.value, det_laser_power, det_dia, det_sacc0, det_sopo0, det_sops0]
        else:
            det_setup=[8.3391, 2., 0.3]
        #for ipop in populations:
        #print ('Population: '+mHz_models[populations]+'<br>')
        t=Tools(Tobs=Tyears, popID=populations, cosmos=cosmos, det_setup=det_setup)
        #print(__name__)
        #if __name__=='__main__':
        #    freeze_support()
        df, tot_num = t.dataframe(rho_cri=snr ,size=sample_size)
       # except:
       #     print("some thing wrong in the t.dataframe step!")
    #tot_num=t.total(Tobs=Tyears,rho_cri=snr)
        #print ("%d %s sources observed!" % (tot_num, mHz_models[populations])+'<br>')
        #tock=time.time()
    #time_totalnum=tock-tick
        sample_size_u=min(sample_size,int(tot_num))
        #print ('Catalog of the first %d sources:' % sample_size_u+'<br>')
        #print('<div id="" style="overflow-y: scroll; overflow-x:scroll; height:200px;">')
        #print(df.to_html(float_format=lambda x: '%.2f' % x))
        #print('</div>')
        response['events']=json.loads(df.to_json(orient='records'))
        response['total']=tot_num
        #tick = time.time()
        #time_cat = tick - tock
        #print("initialize: %f, read in form %f, set cosmology %f, calculate total number %f, catalogue %f" % (time_initialize,time_readinform, time_setoutput_cosmos, time_totalnum, time_cat))
        
        #html_end()
    elif output_table2:
        tick=time.time()
    #  if len(populations) == 0 or len(desired_columns) == 0:
    #    html_start()
    #    print ('Choose source population(s) and/or parameter(s)!')
    #    html_end()
    #    quit()      
        #html_start()
        #print ('Input: <br>')  
        #print ('Detector: LISA-Like<br>')  
        #plotsensecurve()
        #print ('<br>')
        #print ('SNR: '+str(snr)+'<br>')
        Tyears = obs_time
        #print ('Time of observation: %f years' % Tyears+'<br>')
        if Tyears < 0.1 or Tyears > 10:
            html_start()
            print ('Please set observing time between 0.1 and 10 years.')
            html_end()
            quit()
        #print ('Output: <br>')
        if (detector_id < 0):
            det_setup=[det_arm / c.value, det_laser_power, det_dia, det_sacc0, det_sopo0, det_sops0]
        else:
            det_setup=[8.3391, 2., 0.3]
        #for ipop in populations:
        #print ('Population: '+mHz_models[populations]+'<br>')
        t=Tools(Tobs=Tyears, popID=populations, cosmos=cosmos, det_setup=det_setup)
        #print("sample_size=", sample_size, "snr", snr,"populations",populations)
        #df, tot_num = t.dataframe(Tobs=Tyears, rho_cri=snr ,size=sample_size)
        df, tot_num=t.errordataframe(rho_cri=snr, size=sample_size)
            #df, tot_num=t.errordataframe(Tobs=1,rho_cri=10, size=10)
    
            #print("something went wrong in t.errordataframe")
        #print ("%d %s sources observed!" % (tot_num, mHz_models[populations])+'<br>')
        tock=time.time()
        time_totalnum=tock-tick
        sample_size_u=min(sample_size,int(tot_num))
        #print ('Catalog of the first %d sources:' % sample_size_u+'<br>')

        # we have to make the conversion from 'name' to 'index' somewhere so here it is:
        # depending on if we need errors, we can one or the other function
        # in either case we convert to a pandas dataframe immediately
        # so we never have to worry about column names from here on.
    #    columns_err = ['mass1', 'mass2', 'z', 'chi1', 'chi2', 'phi1', 'phi2',
    #                   'tc', 'd', 'lambda', 'beta', 'psi', 'mass1_err', 'mass2_err',
    #                   'chi1_err', 'chi2_err', 'd_err', 'omega_err', 'rho']
    #    columns_normal = ['mass1', 'mass2', 'z', 'chi1', 'chi2', 'd', 'tc', 'psi',
    #                      'lambda', 'beta', 'initPAL', 'initAAL', 'rho']
            
        #if set(desired_columns).issubset(set(columns_normal)):
        #    data = t.list(Tobs=Tyears, rho_cri=snr ,size=sample_size_u)
        #    dataframe = pd.DataFrame(data, columns=columns_normal)
        #else:
        #    data = t.errorlist(Tobs=Tyears, rho_cri=snr ,size=sample_size_u)
        #    dataframe = pd.DataFrame(data, columns=columns_err)
        #df=t.errordataframe(Tobs=Tyears, rho_cri=snr ,size=sample_size_u)
        # print the desireable columns as html
        #print('<div id="" style="overflow-y: scroll; overflow-x:scroll; height:200px;">')
        #print(df.to_html(float_format=lambda x: '%.2f' % x))
        #print('</div>')
        response['events']=json.loads(df.to_json(orient='records'))
        response['total']=tot_num
        #tick = time.time()
        #time_cat = tick - tock
        #iprint("initialize: %f, read in form %f, set cosmology %f, calculate total number %f, catalogue %f" % (time_initialize,time_readinform, time_setoutput_cosmos, time_totalnum, time_cat))
        #html_end()
    else:
        quit()
    format_response(response)
    #html_end()
